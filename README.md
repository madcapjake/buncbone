# Buncbone

Buncbone is a modern CoffeeScript library that provides MVC classes for building web applications.

* Foundation made of a transcribed Backbone.js into CoffeeScript
* Replace jQuery and underscore dependencies with performant modern builtins
* Complement modern 2.x CoffeeScript abstractions and semantics

[Check out the docs](/docs/README.md) for more info.

```coffee
import { Model, Collection, Presenter } from 'buncbone'
import template from 'service-item.mustache'

class Service extends Model
    defaults:
        title: 'My service'
        price: 100
        checked: no
    toggle: -> @set 'checked', not @get('checked')

ServiceList extends Collection
    model: Service
    getChecked: -> @where checked: yes
# Prefill the collection with a number of services.
services = new ServiceList [
    new Service title: 'web development', price: 200
    new Service title: 'web design', price: 250
    new Service title: 'photography', price: 100
    new Service title: 'coffee drinking', price: 10
    # Add more here
]

# This turns a Service Model into views made of LI items
class ServiceItem extends Presenter
    constructor: -> super(); @view = Mustache.parse(template)

    tagName: 'li'

    finalize: -> @listenTo @model, 'change', @render
    
    render: -> @el.html Mustache.render @view, @model.toJSON()

class App extends Presenter
    # TBD

```

## Usage

Describe how to install

## Changes

Describe what's different from Backbone.js

## Community

List community resources

## License

Buncbone is MIT licensed. See [LICENSE](LICENSE.md).