import path from 'path'
import glob from 'glob'

process.argv.slice(2).forEach (arg) ->
  glob arg, (error, files) ->
    if error then throw error
    require(path.resolve(process.cwd(), file)) for file in files