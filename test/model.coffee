import test from 'tape'

import Model from '../lib/model'
import Collection from '../lib/collection'
import { isEqual } from '../lib/utils'

test 'Model', ({ test }) ->

  setup = ->
    book = new Model {
      id: '1-don-quixote'
      title: 'Don Quixote'
      author: 'Miguel De Cervantes'
      length: 992
    }
    class Library extends Collection then url: -> '/library'
    library = new Library()
    library.add book
    return [book, library]

  test 'Model is importable and a class', ({ plan, equal }) ->

    plan 1

    equal Model::constructor.name, 'Model'

  test 'finalize is called', ({ plan, pass, equal }) ->

    plan 1

    class TestModel extends Model
      finalize: -> pass()
    model = new TestModel()

  test 'finalize with attributes and options', ({ plan, equal }) ->

    plan 1

    class TestModel extends Model
      finalize: (attrs, { one }) -> @one = one
    model = new TestModel({}, { one: 1 })
    equal model.one, 1

  test 'instantiate with parsed attributes', ({ plan, equal }) ->

    plan 1

    class TestModel extends Model
      parse: (attrs) ->
        attrs.value += 1
        return attrs
    model = new TestModel { value: 1 }, parse: true
    equal model.get('value'), 2
  
  test 'initialize', ({ plan, equal }) ->

    plan 1

    class TestModel extends Model
      initialize: -> @one = 1
    model = new TestModel()
    equal model.one, 1

  test 'initialize occurs before the model is set up', ({ plan, equal, notEqual }) ->

    plan 4

    class TestModel extends Model
      initialize: ->
        equal @cid, undefined
        equal @id, undefined
    model = new TestModel id: 'foo'
    equal model.id, 'foo'
    notEqual model.cid, undefined

  test 'parse can return null', ({ plan, equal }) ->

    plan 1

    class TestModel extends Model
      parse: (attrs) ->
        attrs.value += 1
        return null
    model = new TestModel()
    equal Object.keys(model.toJSON()).length, 0
  
  test 'url', ({ plan, equal, throws }) ->
    
    plan 3

    [book, library] = do setup

    book.urlRoot = null
    equal book.url(), '/library/1-don-quixote'
    book.collection.url = '/library/'
    equal book.url(), '/library/1-don-quixote'
    book.collection = null
    throws -> book.url()
  
  test 'url when using urlRoot, and uri encoding', ({ plan, equal }) ->

    plan 2

    class TestModel extends Model then urlRoot: '/collection'
    model = new TestModel()
    equal model.url(), '/collection'
    model.set id: '+1+'
    equal model.url(), '/collection/%2B1%2B'
  
  test 'url when using urlRoot as a function to determine urlRoot at runtime', ({ plan, equal }) ->

    plan 2

    class TestModel extends Model then urlRoot: -> "/nested/#{@get 'parentId'}/collection"
    model = new TestModel parentId: 1
    equal model.url(), '/nested/1/collection'
    model.set id: 2
    equal model.url(), '/nested/1/collection/2'

  test 'proxied methods', ({ plan, deepEqual }) ->

    plan 5

    model = new Model foo: 'a', bar: 'b', baz: 'c'
    model2 = model.clone()

    deepEqual model.keys(), ['foo', 'bar', 'baz']
    deepEqual model.values(), ['a', 'b', 'c']
    deepEqual model.invert(), a: 'foo', b: 'bar', c: 'baz'
    deepEqual model.pick('foo', 'baz'), foo: 'a', baz: 'c'
    deepEqual model.omit('foo', 'bar'), baz: 'c'
  
  test 'clone', ({ plan, equal }) ->

    plan 10

    a = new Model foo: 1, bar: 2, baz: 3
    b = a.clone()

    equal a.get('foo'), 1
    equal a.get('bar'), 2
    equal a.get('baz'), 3
    equal b.get('foo'), a.get('foo'), 'Foo should be the same on the clone.'
    equal b.get('bar'), a.get('bar'), 'Bar should be the same on the clone.'
    equal b.get('baz'), a.get('baz'), 'Baz should be the same on the clone.'

    a.set foo: 100

    equal a.get('foo'), 100
    equal b.get('foo'), 1, 'Changing parent attribute does not change clone'

    foo = new Model p: 1
    bar = new Model p: 2
    bar.set foo.clone().attributes, unset: true
    equal foo.get('p'), 1
    equal bar.get('p'), undefined

  test 'isNew', ({ plan, ok, notOk }) ->

    plan 4

    a = new Model foo: 1, bar: 2, baz: 3
    ok a.isNew(), 'it should be new'
    a = new Model foo: 1, bar: 2, baz: 3, id: -5
    notOk a.isNew(), 'any defined ID is legal, negative or positive'
    a = new Model foo: 1, bar: 2, baz: 3, id: 0
    notOk a.isNew(), 'any defined ID is legal, including zero'
    ok new Model().isNew(), 'is true when there is no id'

  test 'get', ({ plan, equal }) ->

    plan 2

    [book, library] = do setup

    equal book.get('title'), 'Don Quixote'
    equal book.get('author'), 'Miguel De Cervantes'
  
  test 'escape', ({ plan, equal }) ->

    plan 5

    [book, library] = do setup

    equal book.escape('title'), 'Don Quixote'
    book.set audience: 'Bill & Bob'
    equal book.escape('audience'), 'Bill &amp; Bob'
    book.set audience: 'Tim > Joan'
    equal book.escape('audience'), 'Tim &gt; Joan'
    book.set audience: 10101
    equal book.escape('audience'), '10101'
    book.unset 'audience'
    equal book.escape('audience'), ''

  test 'check definedness (has / ? / of)', ({ plan, ok, notOk }) ->

    plan 11

    model = new Model()

    notOk model.has 'name'

    model.set
      '0': 0
      '1': 1
      'true': true
      'false': false
      'empty': ''
      'name': 'name'
      'null', null,
      'undefined', undefined
    
    ok model.has '0'
    ok model.get('1')?
    ok 'true' of model
    ok model.has 'false'
    ok model.get('empty')?
    ok 'name' of model

    model.unset('name')

    notOk model.get('name')?
    notOk 'name' of model
    notOk model.has 'null'
    notOk model.get('undefined')?

  test 'matches', ({ plan, equal }) ->

    plan 4

    model = new Model()

    equal model.matches(name: 'Jonas', cool: yes), false

    model.set name: 'Jonas', cool: yes

    equal model.matches(name: 'Jonas'), true
    equal model.matches(name: 'Jonas', cool: yes), true
    equal model.matches(name: 'Jonas', cool: no), false

  test 'matches with predicate', ({ plan, equal }) ->

    plan 2

    func = (attr) -> attr.a > 1 and attr.b?

    model = new Model a: 0

    equal model.matches(func), false

    model.set a: 3, b: yes

    equal model.matches(func), true
  
  test 'set and unset', ({ plan, equal }) ->

    plan 7

    a = new Model id: 'id', foo: 1, bar: 2, baz: 3
    changeCount = 0
    a.on 'change:foo', -> changeCount += 1
    a.set foo: 2
    equal a.get('foo'), 2, 'Foo should have changed'
    equal changeCount, 1, 'Change cound should have incremented'
    a.set foo: 2 # No new value = no change event
    equal a.get('foo'), 2, 'Foo should not have changed'
    equal changeCount, 1, 'Change should should have not incremented'

    a.validate = (attrs) -> equal attrs.foo, undefined, 'validate:true passed while unsetting'
    a.unset 'foo', validate: yes
    equal a.get('foo'), undefined, 'Foo should no longer be defined'
    delete a.validate
    equal changeCount, 2, 'Change count should increment for unset too'
  
  test 'set with failed validate, followed by another set triggers change', ({ plan, deepEqual }) ->

    plan 1

    [attr, main, error] = [0, 0, 0]

    class TestModel extends Model
      validate: (a) -> if a.x > 1 then error++; return 'this is an error'
    model = new TestModel x: 0
    model.on 'change:x', -> attr += 1
    model.on 'change', -> main += 1
    model.set { x: 2 }, validate: yes
    model.set { x: 1 }, validate: yes
    deepEqual [attr, main, error], [1, 1, 1]
  
  test 'set falsy values in the correct order', ({ plan, equal }) ->

    plan 2

    model = new Model result: 'result'
    model.on 'change', ->
      equal model.changed.result, undefined
      equal model.previous('result'), false
    model.set { result: undefined }, silent: yes
    model.set { result: null }, silent: yes
    model.set { result: false }, silent: yes
    model.set result: undefined
  
  test 'nested set triggers with the correct options', ({ plan, equal }) ->

    plan 3

    model = new Model
    [o1, o2, o3] = [{}, {}, {}]
    model.on 'change', ( _, options) ->
      switch model.get('a')
        when 1 then equal options, o1; model.set 'a', 2, o2
        when 2 then equal options, o2; model.set 'a', 3, o3
        when 3 then equal options, o3
    model.set 'a', 1, o1
  
  test 'multiple unsets', ({ plan, equal }) ->

    plan 1

    i = 0
    counter = -> i++
    model = new Model a: 1
    model.on 'change:a', counter
    model.set a: 2
    model.unset 'a'
    model.unset 'a'
    equal i, 2, 'Unset does not fire an event for undefined attributes'
  
  test 'unset and changedAttributes', ({ plan, ok }) ->

    plan 1

    model = new Model a: 1
    fn = -> ok Object.keys(model.changedAttributes()).includes 'a'
    model.on 'change', fn, 'changedAttributes should contain unset properties'
    model.unset 'a'

  test 'using a non-default id attribute', ({ plan, equal }) ->

    plan 5

    class MongoModel extends Model then idAttribute: '_id'
    model = new MongoModel id: 'eye-dee', _id: 25, title: 'Model'
    equal model.get('id'), 'eye-dee'
    equal model.id, 25
    equal model.isNew(), no
    model.unset '_id'
    equal model.id, undefined
    equal model.isNew(), yes

  test 'setting an alternative cid prefix', ({ plan, equal, ok }) ->

    plan 4

    class TestModel extends Model then cidPrefix: 'm'
    model = new TestModel
    equal model.cid.charAt(0), 'm'

    model = new Model
    equal model.cid.charAt(0), 'c'

    class TestCollection extends Collection then model: TestModel
    col = new TestCollection [{ id: 'c5' }, { id: 'c6' }, { id: 'c7' }]
    equal col.get('c6').cid.charAt(0), 'm'

    col.set [{ id: 'c6', value: 'test' }], merge: yes, add: yes, remove: no
    ok col.get('c6').has('value')

    test 'set an empty string', ({ plan, equal }) ->
      
      plan 1

      model = new Model name: 'Model'
      model.set name: ''
      equal model.get('name'), ''
    
    test 'setting an object', ({ plan, pass }) ->
      
      plan 1

      model = new Model custom: foo: 1
      model.on 'change', -> do pass
      model.set custom: foo: 1 # should not fire
      model.set custom: foo: 2 # will fire event


    test 'clear', ({ plan, equal, ok }) ->
      
      plan 3

      changed = no

      model = new Model id: 1, name: 'Model'
      model.on 'change:name', -> changed = yes
      model.on 'change', -> ok Object.keys(model.changedAttributes()).includes 'name'
      model.clear()
      equal changed, yes
      equal model.get('name'), undefined
    
    test 'defaults', ({ plan, equal }) ->

      plan 9

      class DefaultModel extends Model then defaults: one: 1, two: 2
      model = new DefaultModel two: undefined
      equal model.get('one'), 1
      equal model.get('two'), 2
      model = new DefaultModel two: 3
      equal model.get('one'), 1
      equal model.get('two'), 3
      
      class DefaultModel extends Model then defaults: -> one: 3, two: 4
      model = new DefaultModel two: undefined
      equal model.get('one'), 3
      equal model.get('two'), 4

      class DefaultModel extends Model then defaults: hasOwnProperty: yes
      model = new DefaultModel
      equal model.get('hasOwnProperty'), yes
      model = new DefaultModel hasOwnProperty: undefined
      equal model.get('hasOwnProperty'), yes
      model = new DefaultModel hasOwnProperty: no
      equal model.get('hasOwnProperty'), no
    
    test 'change, hasChanged, changedAttributes, ' +
         'previous, previousAttributes', ({ plan, deepEqual, equal, ok, notOk }) ->
      
      plan 9

      model = new Model name: 'Tim', age: 10
      deepEqual model.changedAttributes(), no
      
      model.on 'change', ->
        ok model.hasChanged('name'), 'name changed'
        notOk model.hasChanged('age'), 'age did not'
        ok isEqual(model.changedAttributes(), name: 'Rob'),
          'changedAttributes returns the changed attrs'
        equal model.previous('name'), 'Tim'
        ok isEqual(model.previousAttributes(), name: 'Tim', age: 10),
          'previousAttributes returns the correct last state'
      equal model.hasChanged(), no, 'nothing has changed yet, hasChanged returns false'
      equal model.hasChanged(undefined), no
      model.set name: 'Rob'
      equal model.get('name'), 'Rob'
    
    test 'changedAttributes', ({ plan, equal, deepEqual }) ->

      plan 3

      model = new Model a: 'a', b: 'b'
      deepEqual model.changedAttributes(), no
      equal model.changedAttributes(a: 'a'), no, 'nothing is changed in the diff'
      equal model.changedAttributes(a: 'b').a, 'b', 'now "a" contains a value of "b"'
    
    test 'change with options', ({ plan, equal }) ->

      plan 2

      value = undefined

      model = new Model name: 'Rob'
      model.on 'change', (m, opts) -> value = opts.prefix + m.get 'name'
      model.set { name: 'Bob' }, prefix: 'Mr. '
      equal value, 'Mr. Bob'
      model.set { name: 'Sue' }, prefix: 'Ms. '
      equal value, 'Ms. Sue'
    
    test 'change after initialize', ({ plan, equal }) ->

      plan 1

      [changed, attrs] = [0, id: 1, label: 'c']
      model = new Model attrs
      model.on 'change', -> changed += 1
      model.set attrs
      equal changed, 0, 'attrs are the same so no change should have occurred'

