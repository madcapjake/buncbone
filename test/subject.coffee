import test from 'tape'
{ skip } = test

import Subject from '../lib/subject'

test 'Subject', ({ test }) ->
  
  test 'on and trigger', ({ plan, equal }) ->

    plan 2

    obj = new Subject()
    obj.counter = 0

    obj.on 'event', -> obj.counter += 1

    obj.trigger 'event'

    equal obj.counter, 1, 'counter should be incremented.'

    obj.trigger 'event'
    obj.trigger 'event'
    obj.trigger 'event'
    obj.trigger 'event'

    equal obj.counter, 5, 'counter should be incremented 5 times'

  test 'binding and triggering multiple events', ({ plan, equal }) ->

    plan 4

    obj = new Subject()
    obj.counter = 0

    obj.on 'a b c', -> obj.counter += 1

    obj.trigger 'a'
    equal obj.counter, 1

    obj.trigger 'a b'
    equal obj.counter, 3

    obj.trigger 'c'
    equal obj.counter, 4

    obj.off 'a c'
    obj.trigger 'a b c'
    equal obj.counter, 5
    
  test 'binding and triggering with event maps', ({ plan, equal }) ->

    plan 4

    obj = new Subject()
    obj.counter = 0

    increment = -> @counter += 1

    obj.on
      a: increment
      b: increment
      c: increment
    , obj
    
    obj.trigger 'a'
    equal obj.counter, 1

    obj.trigger 'a b'
    equal obj.counter, 3

    obj.trigger 'c'
    equal obj.counter, 4

    obj.off
      a: increment
      b: increment
    , obj

    obj.trigger 'a b c'
    equal obj.counter, 5
  
  test 'binding and triggering multiple event names with event maps', ({ plan, equal, }) ->

    plan 4

    obj = new Subject()
    obj.counter = 0

    increment = -> @counter += 1

    obj.on
      'a b c': increment
    
    obj.trigger 'a'
    equal obj.counter, 1

    obj.trigger 'a b'
    equal obj.counter, 3

    obj.trigger 'c'
    equal obj.counter, 4

    obj.off
      'a c': increment
    
    obj.trigger 'a b c'
    equal obj.counter, 5

  test 'binding and trigger with event maps context', ({ plan, deepEqual }) ->

    plan 2

    obj = new Subject()
    obj.counter = 0
    context = {}

    obj.on(
      a: -> deepEqual this, context, 'defaults `context` to `callback` param'
    , context).trigger 'a'

    obj.off().on(
      a: -> deepEqual this, context, 'will not override explicit `context` param'
    , context).trigger 'a'
  
  test 'listenTo and stopListening', ({ end, pass, fail }) ->

    a = new Subject()
    b = new Subject()

    a.listenTo b, 'all', -> pass("a's callback is run when b triggers")
    b.trigger 'blah'
    a.listenTo b, 'all', -> fail('stopListening will prevent this from happening')
    a.stopListening()
    b.trigger 'blah'

    end()
  
  test 'listenTo and stopListening with event maps', ({ plan, pass, fail }) ->

    plan 4

    a = new Subject()
    b = new Subject()

    a.listenTo b, event: -> pass 'the "event" key will map to this callback'
    b.trigger 'event'
    
    cb = -> fail 'this "banana" will be stopped and not called'
    a.listenTo b, banana: cb
    b.on 'banana', -> pass 'this "banana" will stay on'
    a.stopListening b, banana: cb
    b.trigger 'event banana'

    a.stopListening()
    b.trigger 'event banana'
  
  test 'stopListening with omitted args', ({ plan, pass, equal }) ->

    plan 3

    a = new Subject()
    b = new Subject()

    cb = -> pass()

    a.listenTo b, 'apple', cb
    b.on 'apple', cb
    a.listenTo b, 'banana', cb
    a.stopListening null, apple: cb
    b.trigger 'apple banana' # both will trigger once

    b.off() # removes listeners too
    a.listenTo b, 'apple banana', cb
    a.stopListening null, 'apple'
    b.trigger 'apple banana' # banana triggers once
    a.stopListening() # no events left
    b.trigger 'banana'
  
  test 'listenToOnce', ({ plan, equal }) ->

    plan 2

    obj = new Subject()
    Object.assign obj, counterA: 0, counterB: 0

    incrA = -> obj.counterA += 1; obj.trigger 'event'
    incrB = -> obj.counterB += 1

    obj.listenToOnce obj, 'event', incrA
    obj.listenToOnce obj, 'event', incrB

    obj.trigger 'event'

    equal obj.counterA, 1, 'counterA should have only been incremented once.'
    equal obj.counterB, 1, 'counterB should have only been incremented once.'
  
  test 'listenTo, listenToOnce and stopListening', ({ plan, pass, fail }) ->

    plan 1

    a = new Subject()
    b = new Subject()

    a.listenToOnce b, 'all', -> pass()
    b.trigger 'anything'
    b.trigger 'foobar'
    a.listenTo b, 'all', -> fail()
    a.stopListening()
    b.trigger 'bazquux'
  
  test 'listenTo and stopListening with event maps', ({ plan, pass, fail }) ->

    plan 1

    a = new Subject()
    b = new Subject()

    a.listenTo b, change: -> pass()
    b.trigger 'change'
    a.listenTo b, change: -> fail()
    a.stopListening()
    b.trigger 'change'

  test 'listenTo yourself', ({ plan, pass }) ->

    plan 1

    e = new Subject()
    e.listenTo e, 'foo', -> pass()
    e.trigger 'foo'
  
  test 'listenTo yourself cleans yourself up with stopListening', ({ plan, pass }) ->

    plan 1

    e = new Subject()
    e.listenTo e, 'foo', -> pass()
    e.trigger 'foo'
    e.stopListening()
    e.trigger 'foo'

  test 'stopListening cleans up references', ({ plan, equal }) ->

    plan 12

    a = new Subject()
    b = new Subject()

    fn = ->
    size = (o) -> Object.keys(o).length

    b.on 'event', fn
    a.listenTo(b, 'event', fn).stopListening()
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenTo(b, 'event', fn).stopListening b
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenTo(b, 'event', fn).stopListening b, 'event'
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenTo(b, 'event', fn).stopListening b, 'event', fn
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
  
  test 'stopListening cleans up references from listenToOnce', ({ plan, equal }) ->

    plan 12

    a = new Subject()
    b = new Subject()

    fn = ->
    size = (o) -> Object.keys(o).length

    b.on 'event', fn

    a.listenToOnce(b, 'event', fn).stopListening()
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenToOnce(b, 'event', fn).stopListening b
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenToOnce(b, 'event', fn).stopListening b, 'event'
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
    a.listenToOnce(b, 'event', fn).stopListening b, 'event', fn
    equal size(a._listeningTo), 0
    equal size(b._events.event), 1
    equal size(b._listeners), 0
  
  test 'listenTo and off cleaning up references', ({ plan, equal }) ->

    plan 8

    a = new Subject()
    b = new Subject()
    fn = ->

    a.listenTo b, 'event', fn
    b.off()
    equal Object.keys(a._listeningTo).length, 0, 'Test 1'
    equal Object.keys(b._listeners).length, 0, 'Test 2'

    a.listenTo b, 'event', fn
    b.off 'event'
    equal Object.keys(a._listeningTo).length, 0, 'Test 3'
    equal Object.keys(b._listeners).length, 0, 'Test 4'

    a.listenTo b, 'event', fn
    b.off null, fn
    equal Object.keys(a._listeningTo).length, 0, 'Test 5'
    equal Object.keys(b._listeners).length, 0, 'Test 6'

    a.listenTo b, 'event', fn
    b.off null, null, a
    equal Object.keys(a._listeningTo).length, 0, 'Test 7'
    equal Object.keys(b._listeners).length, 0, 'Test 8'

  test 'listenTo and stopListening cleaning up references', ({ plan, pass, fail , equal }) ->

    plan 2

    a = new Subject()
    b = new Subject()

    a.listenTo b, 'all', -> pass()
    b.trigger 'anything'
    a.listenTo b, 'other', -> fail()
    a.stopListening b, 'other'
    a.stopListening b, 'all'
    equal Object.keys(a._listeningTo).length, 0
  
  test 'listenToOnce w/o context cleans up refs after event has fired', ({ plan, pass, equal }) ->

    plan 2

    a = new Subject()
    b = new Subject()

    a.listenToOnce b, 'all', -> pass()
    b.trigger 'anything'
    equal Object.keys(a._listeningTo).length, 0

  test 'listenToOnce with event maps cleans up references', ({ plan, pass, equal }) ->

    plan 4

    a = new Subject()
    b = new Subject()

    a.listenToOnce b,
      one: -> pass()
      two: -> pass()
    b.trigger 'one'
    equal Object.keys(a._listeningTo).length, 1
    b.trigger 'two'
    equal Object.keys(a._listeningTo).length, 0
  
  test 'listenToOnce with event maps binds the correct `this`', ({ plan, fail, equal }) ->

    plan 1

    a = new Subject()
    b = new Subject()

    a.listenToOnce b,
      one: -> equal this, a
      two: -> fail()
    b.trigger 'one'
  
  test "listenTo with empty callback doesn't throw an error", ({ end }) ->

    s = new Subject()
    s.listenTo s, 'foo', null
    s.trigger 'foo'
    end()

  test 'trigger all for each event', ({ plan, ok, equal }) ->

    plan 3

    [a, b, subj] = [no, no, new Subject()]
    subj.counter = 0

    subj.on 'all', (event) ->
      subj.counter += 1
      if event is 'a' then a = yes
      if event is 'b' then b = yes
    subj.trigger 'a b'
    ok a
    ok b
    equal subj.counter, 2
  
  test 'on, then unbind all functions', ({ plan, equal }) ->

    plan 1

    class Counter extends Subject then counter: 0
    subj = new Counter()

    callback = -> subj.counter += 1
    subj.on 'event', callback
    subj.trigger 'event'
    subj.off 'event'
    subj.trigger 'event'
    equal subj.counter, 1, 'counter should have only been incremented once'

  test 'bind two callbacks, unbind only one', ({ plan, equal }) ->

    plan 2

    class Counter extends Subject then a: 0, b: 0
    subj = new Counter()
    callback = -> subj.a += 1
    subj.on 'event', callback
    subj.on 'event', -> subj.b += 1
    subj.trigger 'event'
    subj.on 'event'
    subj.off 'event', callback
    subj.trigger 'event'
    equal subj.a, 1, 'a should have only been incremented once'
    equal subj.b, 2, 'b was never turned off so was incremented twice'
  
  test 'unbind a callback in the midst of it firing', ({ plan, equal }) ->

    plan 1

    class Counter extends Subject then counter: 0
    subj = new Counter()
    callback = ->
      subj.counter += 1
      subj.off 'event', callback
    subj.on 'event', callback
    subj.trigger 'event'
    subj.trigger 'event'
    subj.trigger 'event'
    equal subj.counter, 1, 'the callback should have been unbound'
  
  test 'two binds that unbind themeselves', ({ plan, equal }) ->

    plan 2

    class Counter extends Subject then a: 0, b: 0
    subj = new Counter()
    incrA = -> subj.a += 1; subj.off 'event', incrA
    incrB = -> subj.b += 1; subj.off 'event', incrB
    subj.on 'event', incrA
    subj.on 'event', incrB
    subj.trigger 'event'
    subj.trigger 'event'
    subj.trigger 'event'
    equal subj.a, 1, 'a should have only been incremented once'
    equal subj.b, 1, 'b should have only been incremented once'
  
  test 'bind a callback with a default context when none supplied', ({ plan, equal }) ->

    plan 1

    class Asserter extends Subject
      init: (val) -> @value = val
      assert: -> equal this, @value, '`this` was bound to callback'
    subj = new Asserter()
    subj.init(subj)
    subj.once 'event', subj.assert
    subj.trigger 'event'
    subj.trigger 'event'

  skip 'bind a callback with a supplied context', ({ plan, pass }) ->

    plan 1

    class TestClass
      passTest: -> pass '`this` was bound to the callback'
    
    subj = new Subject()
    subj.on 'event', (-> @passTest), new TestClass
    subj.trigger 'event'