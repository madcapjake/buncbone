escape = (string = '') ->
  String(string).replace /[&<>"'\/]/g, (match) -> switch match
    when '&' then '&amp;'
    when '<' then '&lt;'
    when '>' then '&gt;'
    when '"' then '&quot;'
    when "'" then '&#x27;'
    when '/' then '&#x2F;'

isEqual = (x, y) ->
  if x is y then return yes

  [xArray, yArray] = [Array.isArray(x), Array.isArray(y)]

  switch
    when xArray and yArray
      if x.length isnt y.length then return no
      for i in [o...x.length]
        return no unless isEqual x[i], y[i]
      return yes
    when xArray isnt yArray then return no
    when x and y and typeof x is 'object' and typeof y is 'object'
      keys = Object.keys(x)
      if keys.length isnt Object.keys(y).length then return no
      
      [xDate, yDate] = [x, y].map (i) -> i instanceof Date
      if xDate and yDate then return x.getTime() is y.getTime()
      if xDate isnt yDate then return no

      [xRegExp, yRegExp] = [x, y].map (i) -> i instanceof RegExp
      if xRegExp and yRegExp then return x.toString() is y.toString()
      if xRegExp isnt yRegExp then return no

      for key in keys
        return no unless Object::hasOwnProperty.call y, key
      
      for key in keys
        return no unless isEqual x[key], y[key]
      
      return yes
    else return no

isFunction = (object) ->
  !!(object and object.constructor and object.call and object.apply)

# An object with keys for each prefix and a count of each id as value
ids = {}

# A symbol to represent a count of all ids without a prefix
unprefixed = Symbol('unprefixed')

uid = (prefix) ->
  prefix = unprefixed unless prefix?
  ids[prefix] ?= 0
  if prefix? then "#{prefix}-#{ids[prefix]++}" else "#{ids[prefix]++}"

class URLError extends Error
  constructor: (msg = 'A "url" property or function must be specified') -> super(msg)

# Override this function to change the manner in which Buncbone persists
# models to the server. You will be passed the type of request, and the model in
# question. By default, makes a RESTful fetch request to the model’s url(). Some
# possible customizations could be:
#
# * Use setTimeout to batch rapid-fire updates into a single request.
# * Send up the models as XML instead of JSON.
# * Persist models via WebSockets instead of fetch.
#
# Currently not implementing the old server support as I want to first get the
# fetch api working.
sync = (crud, model, options) ->
  method = switch crud
    when 'update' then 'PUT'
    when 'create' then 'POST'
    when 'patch' then 'PATCH'
    when 'delete' then 'DELETE'
    when 'read' then 'GET'

  params = { method, headers: new Headers() }

  # Ensure that we have a URL.
  url = null
  if options.url?
    url = options.url
    delete options['url']
  else url = switch typeof model.url
    when 'function' then model.url()
    when 'string' then model.url
    else throw new URLError()
  request = new Request(url)
  
  # Ensure that we have the appropriate request data.
  if model? and ['create', 'update', 'patch'].includes crud
    params.headers.append 'Content-Type', 'application/json'
    params.body = JSON.stringify options.attrs ? model.toJSON(options)

  fetching = fetch request, { params..., options... }

  # Check status code
  fetching.then (response) ->
    if 200 <= response.status < 300 then return Promise.resolve response
    else return Promise.reject new Error(response.statusText)

  # Convert data to json
  fetching.then (response) -> return Promise.resolve response.json()

  if options.success? then fetching.then (data) ->
    options.data = data
    options.success.call options.context, fetching, data
    return data

  userErrorCB = options.error
  fetching.catch (error) ->
    options.error = error
    if userErrorCB? then userErrorCB.call options.context, fetching, error

  model.trigger 'request', model, fetching, options
  return fetching

export { escape, isEqual, isFunction, uid, URLError, sync }