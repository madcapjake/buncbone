import { escape, isEqual, uid, URLError, sync } from './utils'
import Subject from './subject'

class Model extends Subject

  constructor: (attrs = {}, options = {}) ->
    super()
    @initialize.apply(this, arguments)

    @cid = uid @cidPrefix
    @attributes = {}
    @changed = {}
    @collection = options.collection
    
    if options.parse then attrs = @parse(attrs, options) ? {}

    defaults = switch typeof @defaults
      when 'function' then @defaults()
      when 'object' then @defaults
      else {}
    # remove key when there is no value provided
    delete attrs[key] for key in Object.keys(attrs) when not attrs[key]?
    attrs = { defaults..., attrs... }

    @set(attrs, options)

    # Don't consider the first attributes to be changes
    @changed = {}

    @finalize.apply(this, arguments)

    return new Proxy this, has: (target, prop) -> prop of target.attributes

  validationError: null

  idAttribute: 'id'

  cidPrefix: 'c'

  # initialize is an empty function by default. You can override it with a
  # function or object.  initialize will run before any instantiation logic
  # is run in the Model.
  initialize: ->

  # finalize is an empty function by default. Override this to do anything after
  # instantiation logic is complete.
  finalize: ->

  toJSON: -> Object.assign({}, @attributes)

  sync: (params...) -> sync.call(this, params...)

  get: (attr) -> @attributes[attr]

  escape: (attr) -> escape @get attr

  has: (attr) -> @get(attr)?

  # Checks if model has all attrs and values that attrs contains
  matches: (attrs) ->
    return attrs(@attributes) if typeof attrs is 'function'
    matchObj = (objA, objB) ->
      for key, val of objB
        return no unless key in Object.keys(objA)
        if typeof val is 'object'
          if val.constructor isnt Array
            return no unless m objA[key], val
          else for el, i in val
            return no unless el is objA[key][i]
        else return no unless val is objA[key]
      return yes
    return matchObj @attributes, attrs

  # Decorate each method that is triggered by an event and it will add the
  # method to the @events map
  via: (eventName, func) ->
    @events ?= {}
    @events[eventName] = func
    return func

  set: (key, val, options) ->
    return this if key is null

    # Handle both "key", value and {key: value} -style arguments.
    [attrs, options] = if typeof key is 'object' then [key, val]
    else [Object.assign({}, [key]: val), options]

    options ?= {}

    # Run validation
    return no unless validate.call this, attrs, options

    # Extract attributes and options.
    { unset, silent } = options
    changes = []
    changing = @_changing
    @_changing = yes

    unless changing
      @_previousAttributes = Object.assign {}, @attributes
      @changed = {}
    
    current = @attributes
    changed = @changed
    prev = @_previousAttributes

    # For each set attribute, update or delete the current value.
    for attr, val of attrs
      changes.push attr unless isEqual current[attr], val
      if not isEqual prev[attr], val then changed[attr] = val else delete changed[attr]
      if unset then delete current[attr] else current[attr] = val
    
    # Update the id
    @id = @get(@idAttribute) if Object.keys(attrs).includes @idAttribute

    # Trigger all relevant attribute changes.
    unless silent
      @_pending = options if changes.length
      for change in changes
        @trigger "change:#{change}", this, current[change], options
    
    # You might be wondering why there’s a while loop here. Changes can be
    # recursively nested within "change" events.
    return this if changing
    unless silent
      while @_pending
        options = @_pending
        @_pending = no
        @trigger 'change', this, options
    @_pending = no
    @_changing = no
    return this

  # Remove an attribute from the model, firing "change". unset is a noop if the
  # attribute doesn’t exist.
  unset: (attr, options) -> @set attr, undefined, { options..., unset: yes }

  # Clear all attributes on the model, firing "change".
  clear: (options) ->
    attrs = {}; attrs[key] = undefined for key in Object.keys(@attributes)
    @set attrs, { options..., { unset: true }... }
  
  # Determine if the model has changed since the last "change" event. If you
  # specify an attribute name, determine if that attribute has changed.
  hasChanged: (attr) -> if attr? then @changed[attr]? else !!Object.keys(@changed).length
  
  # Return an object containing all the attributes that have changed, or false
  # if there are no changed attributes. Useful for determining what parts of a
  # view need to be updated and/or what attributes need to be persisted to the
  # server. Unset attributes will be set to undefined. You can also pass an
  # attributes object to diff against the model, determining if there would be a
  # change.
  changedAttributes: (diff) ->
    if not diff? then return (if @hasChanged() then { @changed... } else no)
    old = if @_changing then @_previousAttributes else @attributes
    changed = {}
    changed[attr] = val for attr, val of diff when not isEqual old[attr], val
    return if Object.keys(changed).length isnt 0 then changed else false
  
  # Get the previous value of an attribute, recorded at the time the last
  # "change" event was fired.
  previous: (attr) ->
    unless attr? or @_previousAttributes? then return false
    return @_previousAttributes[attr]
  
  # Get all of the attributes of the model at the time of the previous "change"
  # event.
  previousAttributes: -> Object.assign {}, @_previousAttributes

  # Fetch the model from the server, merging the response with the model’s local
  # attributes. Any changed attributes will trigger a “change” event.
  fetch: (options) ->
    defaults = { parse: yes }
    options = { defaults, options... }
    { success, error, context, parse } = options
    options.success = (data) =>
      serverAttrs = if parse then @parse(data, options) else data
      return no unless @set serverAttrs, options
      success.call context, this, data, options if success?
      @trigger 'sync', this, data, options
    options.error = (data) =>
      error.call context, this, data, options if error?
      @trigger 'error', this, data, options
    return @sync 'read', this, options

  # Set a hash of model attributes, and sync the model to the server. If the
  # server returns an attributes hash that differs, the model’s state will be
  # set again.
  save: (key, val, options) ->

    # Handle both "key", value and {key: value} -style arguments.
    [attrs, options] = if typeof key is 'object' then [key, val]
    else [Object.assign({}, [key]: val), options]

    options = { validate: yes, parse: yes, options... }
    { wait, success, error } = options

    # If we’re not waiting and attributes exist, save acts as
    # set(attr).save(null, opts) with validation. Otherwise, check if the model
    # will be valid when the attributes, if any, are set.
    if attrs and not wait then return no unless @set attrs, options
    else unless validate.call this, attrs, options then return no

    # After a successful server-side save, the client is (optionally) updated
    # with the server-side state.
    # XXX below isn't necessary due to =>, right?
    # model = this
    attributes = @attributes
    options.success = (resp) =>
      # Ensure attributes are restored during synchronous saves.
      # XXX can this be solved by binding?
      @attributes = attributes
      serverAttrs = if options.parse then @parse resp, options else resp
      serverAttrs = { attrs..., serverAttrs... } if wait
      return no if serverAttrs and not @set serverAttrs, options
      success.call options.context, this, resp, options if success?
      @trigger 'sync', this, resp, options
    
    # Wrap an optional error callback with a fallback error event.
    options.error = (resp) =>
      error.call options.context, this, resp, options if error?
      @trigger 'error', this, resp, options
    
    # Set temporary attributes if {wait: true} to properly find new ids.
    if attrs and wait then @attributes = { attributes..., attrs... }

    method = switch
      when @isNew() then 'create'
      when options.patch
        unless options.attrs? then options.attrs = attrs
        'patch'
      else 'update'
    xhr = @sync method, this, options

    # Restore attributes? But it's a reference!
    @attributes = attributes

    return xhr
    
  # Destroy this model on the server if it was already persisted. Optimistically
  # removes the model from its collection, if it has one. If wait: true is
  # passed, waits for the server to respond before removal.
  destroy: (options) ->
    options = if options then Object.assign({}, options) else {}
    { success, error, wait, context } = options

    destroy = =>
      @stopListening()
      @trigger 'destroy', this, @collection, options
    
    options.success = (resp) =>
      destroy() if wait
      success.call context, this, resp, options if success?
      @trigger 'sync', this, resp, options unless @isNew()
    
    xhr = no
    if @isNew() then setTimeout options.success, 1
    else
      options.error = (resp) =>
        error.call context, this, resp, options if error?
        @trigger 'error', this, resp, options
      xhr = @sync 'delete', this, options
    destroy() unless wait
    return xhr
  
  # Default URL for the model’s representation on the server – if you’re using
  # Backbone’s restful methods, override this to change the endpoint that will
  # be called.
  url: ->
    base = switch
      when @urlRoot?
        if typeof @urlRoot is 'function' then @urlRoot() else @urlRoot
      when @collection?.url?
        if typeof @collection.url is 'function' then @collection.url() else @collection.url
      else throw new URLError()
    return base if @isNew()
    id = @get(@idAttribute)
    return base.replace(/[^\/]$/, '$&/') + encodeURIComponent id
  
  # parse converts a response into the hash of attributes to be set on the
  # model. The default implementation is just to pass the response along.
  parse: (resp, options) -> resp

  # Create a new model with identical attributes to this one.
  clone: -> new @constructor(@attributes)

  # A model is new if it has never been saved to the server, and lacks an id.
  isNew: -> not @get(@idAttribute)?

  # Check if the model is currently in a valid state.
  isValid: (options) -> validate.call this, {}, { options..., validate: true }

  # Run validation against the next complete set of model attributes, returning
  # true if all is well. Otherwise, fire an "invalid" event.
  validate = (attrs, options) ->
    unless options.validate and @validate then return yes
    attrs = { @attributes..., attrs... }
    error = @validationError = @validate(attrs, options) or null
    return yes unless error?
    @trigger 'invalid', this, error, { options..., validationError: error }
    return no

  keys: -> Object.keys(@attributes)

  values: -> Object.values(@attributes)

  pairs: -> [key, @get(key)] for key in @keys()

  invert: -> @keys().reduce ((res, key) => { res..., [@get(key)]: key }), {}
  
  pick: (keys...) ->
    keys = keys[0] if keys.length is 1 and keys[0].constructor.name is 'Array'
    return keys.reduce ((res, key) => { res..., [key]: @get(key) }), {}

  omit: (keys...) ->
    keys = keys[0] if keys.length is 1 and keys[0].constructor.name is 'Array'
    return keys.reduce ((res, key) -> delete res[key]; res), @toJSON()

  isEmpty: -> Object.keys(@attributes).length > 0

export default Model
