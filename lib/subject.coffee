import { uid } from './utils'

# Regular expression used to split event strings.
eventSplitter = /\s+/

# Iterates over the standard event, callback (as well as the fancy multiple
# space-separated events "change blur", callback and jQuery-style event maps
# {event: callback}).
eventsAPI = (iteratee, events, name, callback, opts) ->
  [i, names] = [0, null]

  switch
    # Handle event maps
    when name? and typeof name is 'object'
      if callback? and not opts.context? then opts.context = callback
      for key in Object.keys(name)
        events = eventsAPI iteratee, events, key, name[key], opts
    # Handle space-separated event names by delegating them individually.
    when name? and eventSplitter.test name
      for fragment in name.split(eventSplitter)
        events = iteratee events, fragment, callback, opts
    # Finally, standard events.
    else
      events = iteratee events, name, callback, opts

  return events

# The reducing API that adds a callback to the events object.
onAPI = (events, name, callback, options) ->
  if callback?
    handlers = events[name] ?= []
    { context, ctx, listening } = options
    if listening? then listening.count += 1
    handlers.push { callback, context, ctx: context ? ctx, listening }
  return events

# The reducing API that removes a callback from the events object.
offAPI = (events, name, callback, options) ->
  unless events? then return

  { context, listeners } = options

  # Delete all events listeners and “drop” events.
  if not name? and not callback? and not context?
    for _, listener of listeners
      delete listeners[listener.id]
      delete listener.listeningTo[listener.objId]
    return

  for name in (if name? then [name] else Object.keys(events))
    handlers = events[name]

    # Bail out if there are no events stored.
    break unless handlers?

    # Replace events if there are any remaining. Otherwise, clean up.
    remaining = []
    for handler in handlers
      if callback? and callback isnt handler.callback and
        callback isnt handler.callback._callback or context? and
        context isnt handler.context then remaining.push handler
      else
        listening = handler.listening
        if listening? and --listening.count is 0
          delete listeners[listening.id]
          delete listening.listeningTo[listening.objId]

    # Update tail event if the list has any events. Otherwise, clean up.
    if remaining.length isnt 0 then events[name] = remaining
    else delete events[name]

  return events

# Handles triggering the appropriate event callbacks.
triggerAPI = (objEvents, name, callback, args) ->
  if objEvents?
    events = objEvents[name]
    allEvents = objEvents.all
    if events? and allEvents? then allEvents = allEvents.slice()
    if events? then triggerEvents events, args
    if allEvents? then triggerEvents allEvents, [name].concat(args)
  return objEvents

# A difficult-to-believe, but optimized internal dispatch function for
# triggering events. Tries to keep the usual cases speedy (most internal
# Backbone events have 3 arguments).
triggerEvents = (events, args) ->
  [a1, a2, a3] = [args[0], args[1], args[2]]
  switch args.length
    when 0 then ev.callback.call(ev.ctx) for ev in events
    when 1 then ev.callback.call(ev.ctx, a1) for ev in events
    when 2 then ev.callback.call(ev.ctx, a1, a2) for ev in events
    when 3 then ev.callback.call(ev.ctx, a1, a2, a3) for ev in events
    else ev.callback.call(ev.ctx, args) for ev in events


# Guard the listening argument from the public API.
internalOn = (obj, name, callback, context, listening) ->
  obj._events = eventsAPI onAPI, obj._events ? {}, name, callback,
    context: context
    ctx: obj
    listening: listening

  if listening?
    obj._listeners ?= {}
    obj._listeners[listening.id] = listening

  return obj


once = (func) ->
  result = null
  return -> result ?= func.apply(this, arguments)

# Reduces the event callbacks into a map of {event: onceWrapper}. offer unbinds
# the onceWrapper after it has been called.
onceMap = (map, name, callback, offer) ->
  if callback?
    o = map[name] = once ->
      offer name, o
      callback.apply this, arguments
    o._callback = callback
  return map

class Subject

  constructor: ->

  # Bind an event to a callback function.
  on: (name, callback, context) -> internalOn this, name, callback, context

  # Inversion-of-control version of on. Tell this object to listen to an event
  # in another object… keeping track of what it’s listening to for easier
  # unbinding later.
  listenTo: (obj, name, callback) ->
    unless obj then return this
    you = obj._listenId ?= uid 'l'
    listeningTo = @_listeningTo ?= {}
    listening = listeningTo[you]

    # This object is not listening to any other events on obj yet. Setup the
    # necessary references to track the listening callbacks.
    unless listening?
      me = @_listenId ?= uid 'l'
      @_listeningTo[you] = { obj, listeningTo, objId: you, id: me, count: 0 }
      listening = @_listeningTo[you]

    # Bind callbacks on obj, and keep track of them on listening.
    internalOn obj, name, callback, this, listening
    return this

  # Tell this object to stop listening to either specific events … or to every
  # object it’s currently listening to.
  stopListening: (obj, name, callback) ->
    listeningTo = @_listeningTo
    return this unless listeningTo
    
    for you in (if obj then [obj._listenId] else Object.keys(listeningTo))
      # breaks if listeningTo[you] doesn't exist
      listeningTo[you]?.obj.off name, callback, this

    return this

  # Remove one or many callbacks. If context is null, removes all callbacks with
  # that function. If callback is null, removes all callbacks for the event. If
  # name is null, removes all bound callbacks for all events.
  off: (name, callback, context) ->
    unless @_events? then return this
    @_events = eventsAPI offAPI, @_events, name, callback,
      context: context
      listeners: @_listeners
    return this

  # Bind an event to only be triggered a single time. After the first time the
  # callback is invoked, its listener will be removed. If multiple events are
  # passed in using the space-separated syntax, the handler will fire once for
  # each event, not once for a combination of all events.
  once: (name, callback, context) ->
    # Map the event into a {event: once} object.
    events = eventsAPI onceMap, {}, name, callback, @off.bind(this)
    if typeof name is 'string' and not context? then callback = undefined
    return @on events, callback, context

  # Inversion-of-control versions of once.
  listenToOnce: (obj, name, cb) ->
    events = eventsAPI onceMap, {}, name, cb, @stopListening.bind(this, obj)
    return @listenTo obj, events

  # Trigger one or many events, firing all bound callbacks. Callbacks are passed
  # the same arguments as trigger is, apart from the event name (unless you’re
  # listening on "all", which will cause your callback to receive the true name
  # of the event as the first argument).
  trigger: (name) ->
    unless @_events then return this
    [name, args...] = arguments
    eventsAPI triggerAPI, @_events, name, undefined, args
    return this

export default Subject
