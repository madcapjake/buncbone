import { isFunction, sync } from './utils'
import Subject from './subject'
import Model from './model'

# If models tend to represent a single row of data, a Buncbone Collection is
# more analogous to a table full of data … or a small slice or page of that table,
# or a collection of rows that belong together for a particular reason – all of
# the messages in this particular folder, all of the documents belonging to this
# particular author, and so on. Collections maintain indexes of their models, both
# in order, and for lookup by id.
class Collection extends Subject

  # Private key to access byId map by Symbol
  byId = Symbol('byId')

  # Create a new Collection, perhaps to contain a specific type of model. If a
  # comparator is specified, the Collection will maintain its models in sort
  # order, as they’re added and removed.
  constructor: (models, options = {}) ->

    super()

    @model = options.model if options.model?
    @comparator = options.comparator if options.comparator?

    reset.call(this)

    @finalize.apply(this, arguments)

    if models? then @reset models, { { silent: yes }..., options... }

  setOptions = add: yes, remove: yes, merge: yes
  addOptions = add: yes, remove: no

  # The default model for a collection is just a Buncbone.Model. This should be
  # overridden in most cases.
  model: Model

  # Finalize is an empty function by default. Override it with your own
  # finalization logic.
  finalize: ->

  # The JSON representation of a Collection is an array of the models’ attributes.
  toJSON: (options) -> @map((model) -> model.toJSON(options))

  sync: (params...) -> sync params...

  add: (models, options) -> @set models, { merge: no, options..., addOptions... }

  # Remove a model, or a list of models from the set.
  remove: (models, options) ->
    options = Object.assign({}, options)
    singular = not Array.isArray(models)
    models = if singular then [models] else models.slice()
    removed = removeModels models, options
    if not options.silent and removed.length
      options.change = added: [], merged: [], removed: removed
      @trigger 'update', this, options
    return if singular then removed[0] else removed
  
  # Update a collection by set-ing a new list of models, adding new ones, removing
  # models that are no longer present, and merging models that already exist in the
  # collection, as necessary. Similar to Model#set, the core operation for updating
  # the data contained by the collection.
  set: (models, options) ->
    return unless models?

    options = Object.assign({}, setOptions, options)
    if options.parse and not models instanceof Model
      models = @parse(models, options) ? []
    
    singular = not Array.isArray(models)
    models = if singular then [models] else models[..]

    { at } = options
    at = switch
      when +at > @length then @length
      when +at < 0 then at + @length + 1
      when at? then +at else null

    [set, toAdd, toMerge, toRemove, modelMap] = [[], [], [], [], {}]

    { add, merge, remove } = options

    sort = no
    sortable = @comparator? and at is null and options.sort isnt no
    sortAttr = if typeof @comparator is 'string' then @comparator else null

    # Turn bare objects into model references, and prevent invalid models from
    # being added.
    for model, i in models

      # If a duplicate is found, prevent it from being added and optionally merge
      # it into the existing model.
      existing = @get model
      if existing?
        if merge and model isnt existing
          attrs = if model instanceof Model then model.attributes else model
          attrs = existing.parse(attrs, options) if options.parse
          existing.set attrs, options
          toMerge.push existing
          sort = existing.hasChanged(sortAttr) if sortable and not sort
        unless modelMap[existing.cid]
          modelMap[existing.cid] = yes
          set.push existing
        models[i] = existing
      
      # If this is a new, valid model, push it to the toAdd list.
      else if add
        model = models[i] = prepareModel.call this, model, options
        if model?
          toAdd.push model
          addReference.call this, model, options
          modelMap[model.cid] = yes
          set.push model
    
    # Remove stale models.
    if remove
      toRemove.push model unless modelMap[model.cid] for model in @models
      if toRemove.length isnt 0 then removeModels.call this, toRemove, options
    
    # See if sorting is needed, update length and splice in new models.
    orderChanged = no
    replace = not sortable and add and remove
    if set.length isnt 0 and replace
      orderChanged = @length isnt set.length or @models.some (m, i) -> m isnt set[i]
      @models.length = 0
      @models.splice 0, 0, set...
      @length = @models.length
    else if toAdd.length isnt 0
      sort = yes if sortable
      @models.splice at ? @length, 0, toAdd...
      @length = @models.length
    
    # Silently sort the collection if appropriate.
    @sort silent: yes if sort

    # Unless silenced, it’s time to fire all appropriate add/sort/update events.
    unless options.silent
      for model, i in toAdd
        options.index = at + i if at?
        model.trigger('add', model, this, options)
      if sort or orderChanged then @trigger 'sort', this, options
      if [toAdd, toRemove, toMerge].some((list) -> list.length isnt 0)
        options.change = added: toAdd, removed: toRemove, merged: toMerge
        @trigger 'update', this, options
    
    # Return the added (or merged) model (or models).
    return if singular then models[0] else models
  
  # When you have more items than you want to add or remove individually, you can
  # reset the entire set with a new list of models, without firing any granular
  # add or remove events. Fires reset when finished. Useful for bulk operations
  # and optimizations.
  reset: (models, options = {}) ->
    options = Object.assign({}, options)
    removeReference.call(this, model, options) for model in @models
    options.previousModel = @models
    reset.call(this)
    models = @add models, { silent: yes, options... }
    unless options.silent then @trigger 'reset', this, options
    return models
  
  # Add a model to the end of the collection.
  push: (model, options = {}) -> @add model, { at: @length, options... }
  
  # Remove a model from the end of the collection.
  pop: (model, options) -> @remove @at(@length - 1), options

  # Add a model to the beginning of the collection.
  unshift: (model, options) -> @add model, { at: 0, options... }

  # Remove a model from the beginning of the collection.
  shift: (options) -> @remove @at(0), options

  # Slice out a sub-array of models from the collection.
  slice: -> Array::slice.apply(this, arguments)

  # Get a model from the set by id, cid, model object with id or cid properties, or
  # an attributes object that is transformed through modelId.
  get: (obj) ->
    model = undefined
    return model unless obj?
    return model if (model = @[byId][obj])?
    return model if (model = @[byId][@modelId(obj.attributes ? obj)])?
    return @[byId][obj.cid]
  
  # Returns true if the model is in the collection.
  has: (obj) -> @get(obj)?

  # Get the model at the given index.
  at: (index) -> @models[if index < 0 then index + @length else index]

  # Return models with matching attributes. Useful for simple cases of filter.
  where: (attrs, first) -> @[if first then 'find' else 'filter'](attrs)

  # Return the first model with matching attributes. Useful for simple cases of find.
  findWhere: (attrs) -> @where attrs, yes

  # Force the collection to re-sort itself. You don’t need to call this under normal
  # circumstances, as the set will maintain sort order as each item is added.
  sort: (options = {}) ->
    throw new Error('Cannot sort a set without a comparator') unless @comparator?

    length = @comparator.length
    comparator = @comparator.bind(this) if isFunction(@comparator)

    # Run sort based on the type of comparator
    if length is 1 and typeof comparator is 'string'
      @models = @sortBy comparator
    else @models.sort(comparator)
    
    @trigger('sort', this, options) unless options.silent

    return this
  
  # Pluck an attribute from each model in the collection.
  pluck: (attr) -> @map String attr

  # Fetch the default set of models for this collection, resetting the collection
  # when they arrive. If reset: true is passed, the response data will be passed
  # through the reset method instead of set.
  fetch: (options) ->
    options = { parse: yes, options... }
    { success, error } = options
    options.success = (data) =>
      method = if options.reset then 'reset' else 'set'
      @[method](data, options)
      if success? then success.call options.context, this, data, options
      @trigger 'sync', this, options
    options.error = (data) =>
      if error? then error.call options.context, this, data, options
      @trigger 'error', this, data, options
    return @sync 'read', this, options

  # Create a new instance of a model in this collection. Add the model to the
  # collection immediately, unless wait: true is passed, in which case we wait for
  # the server to agree.
  create: (model, options) ->
    options = Object.assign({}, options)
    { wait, success } = options
    model = prepareModel.call this, model, options
    return false unless model?
    @add(model, options) unless wait
    options.success = (m, resp, callbackOpts) =>
      @add(m, callbackOpts) if wait
      success.call(callbackOpts.context, m, resp, callbackOpts) if success?
    model.save null, options
    return model
  
  # parse converts a response into a list of models to be added to the collection. The
  # default implementation is just to pass it through.
  parse: (resp, options) -> resp

  # Create a new collection with an identical list of models as this one.
  clone: -> new @constructor @models, model: @model, comparator: @comparator

  # Define how to uniquely identify models in the collection.
  modelId: (attrs) -> attrs[@model::idAttribute ? 'id']

  # Private method to reset all internal state. Called when the collection is first
  # initialized or reset.
  reset = ->
    @length = 0
    @models = []
    @[byId] = {}
  
  # Prepare a hash of attributes (or other model) to be added to this collection.
  #*Must be called with .call(this, attrs, options) due to being a private function
  prepareModel = (attrs, options = {}) ->
    if attrs instanceof Model
      attrs.collection = this unless attrs.collection?
      return attrs
    options = Object.assign({}, options)
    options.collection = this
    model = new @model(attrs, options)
    return model unless model.validationError?
    @trigger 'invalid', this, model.validationError, options
    return no
  
  # Internal method called by both remove and set.
  #*Must be called with .call(this, attrs, options) due to being a private function
  removeModels = (models, options = {}) ->
    removed = []
    for toBeRemoved in models
      model = @get toBeRemoved
      continue if not model?
      index = @indexOf model
      @models.splice(index, 1)
      @length -= 1

      # Remove references before triggering ‘remove’ event to prevent an infinite loop.
      delete @[byId][model.cid]
      id = @modelId model.attributes
      if id? then delete @[byId][id]
      unless options.silent
        options.index = index
        model.trigger 'remove', model, this, options
      removed.push model
      removeReference.call this, model, options
    return removed
  
  # Internal method to create a model’s ties to a collection.
  #*Must be called with .call(this, attrs, options) due to being a private function
  addReference = (model, options) ->
    @[byId][model.cid] = model
    id = @modelId model.attributes
    if id? then @[byId][id] = model
    model.on 'all', onModelEvent.bind(this), this
  
  # Internal method to sever a model’s ties to a collection.
  #*Must be called with .call(this, attrs, options) due to being a private function
  removeReference = (model, options) ->
    delete @[byId][model.cid]
    id = @modelId model.attributes
    if id? then delete @[byId][id]
    if this is model.collection then delete model.collection
    model.off 'all', onModelEvent.bind(this), this
  
  # Internal method called every time a model in the set fires an event. Sets need to
  # update their indexes when models change ids. All other events simply proxy through.
  # “add” and “remove” events that originate in other collections are ignored.
  #*Must be called with .call(this, attrs, options) due to being a private function
  onModelEvent = (event, model, collection, options) ->
    switch event
      when 'add', 'remove' then return unless collection is this
      when 'destroy' then @remove(model, options)
      when 'change'
        prevId = @modelId model.previousAttributes()
        id = @modelId model.attributes
        if prevId isnt id
          if prevId? then delete @[byId][prevId]
          if id? then @[byId] = model
    @trigger.apply this, arguments
  
  # Proxy Array:: methods to @models

  entries: -> @models.entries.apply(this)

  every: -> @models.every.apply(this, arguments)
  all: -> @every.apply(this, arguments)

  filter: -> @models.filter.apply(this, arguments)

  find: -> @models.find.apply(this, arguments)

  findIndex: -> @models.find.apply(this, arguments)

  forEach: -> @models.forEach.apply(this, arguments)

  includes: -> @models.includes.apply(this, arguments)

  indexOf: -> @models.indexOf.apply(this, arguments)

  keys: -> @models.indexOf.apply(this, arguments)

  lastIndexOf: -> @models.lastIndexOf.apply(this, arguments)

  map: -> @models.map.apply(this, arguments)

  reduce: -> @models.reduce.apply(this, arguments)

  reduceRight: -> @models.reduceRight.apply(this, arguments)

  some: -> @models.some.apply(this, arguments)
  any: -> @some.apply(this, arguments)

  values: -> @models.values.apply(this, arguments)

  # Underscore-like methods not covered by ES builtins

  max: (iteratee, context = this) ->
    [highest, highestValue, currentValue] = [null, null, null]
    for model in @models
      currentValue = iteratee.call(context, model)
      if highestValue? and currentValue > highestValue
        highestValue = currentValue
        highest = model
      unless highestValue?
        highestValue = currentValue
        highest = model
  
  min: (iteratee, context = this) ->
    [lowest, lowestValue, currentValue] = [null, null, null]
    for model in @models
      currentValue = iteratee.call(context, model)
      if lowestValue? and currentValue < lowestValue
        lowestValue = currentValue
        lowest = model
      unless lowestValue?
        lowestValue = currentValue
        lowest = model
  
  toArray: -> @models[..]

  size: -> @models.length

  first: (n = 1) -> @models[0...n]
  head: -> @first.apply(this, arguments)
  take: -> @first.apply(this, arguments)

  initial: (n = 1) -> @models[0..-n]

  last: (n = 1) -> @models[-n..]

  rest: (n = 1) -> @models[n..]
  tail: -> @rest.apply(this, arguments)
  drop: -> @rest.apply(this, arguments)

  difference: (values) ->
    return @models unless values.some (v) -> v instanceof Model
    @filter (model) -> not values.includes(model)
  without: (values...) -> @difference(values)

  randomInt = (min, max) ->
    [min, max] = [Math.ceil(min), Math.floor(max)]
    return Math.floor(Math.random() * (max - min)) + min

  shuffle: (options) ->
    for i in [0..(@models.length - 2)]
      j = randomInt i, @models.length
      temp = @models[i]
      @models[i] = @models[j]
      @models[j] = temp
    @trigger 'sort', this, options unless options.silent
    return this
  
  isEmpty: -> @models.length isnt 0

export default Collection


